<section class="saffron_aloevera padding-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-lg-5 col-md-7 col-sm-12 col-xs-12">
				<div class="product">
					<div class="pro-1">
						<ul>
							<li onclick="document.getElementById('myImage').src='img/Product-5.jpg'">
								<img src="img/Product-5.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-5-img-2.jpg'">
								<img src="img/Product-5-img-2.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-5-img-3.jpg'">
								<img src="img/Product-5-img-3.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-5-img-4.jpg'">
								<img src="img/Product-5-img-4.jpg" alt="img"/>
							</li>
						</ul>
					</div>
					<div class="pro-2">
						<div class="rectangle"></div>
						<img id="myImage" src="img/Product-5.jpg" alt="img"/>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-lg-7 col-md-5 col-sm-12 col-xs-12">
				<h2>Natural Saffron Aloevera Gel for men & women (SPF 50)</h2>
				<h5>Brand : Rachna Care / Sold by : Mitali Care Plus</h5>
				<h6>Rs. <span>150</span> / Weight : <span>100 ml</span></h6>
				<h6>Reviews 0</h6>
				<div class="line"></div>
				<h6>Expiry Date : 04/2025</h6>
				<h6>Suitable for : All skin Type</h6>
				<h6>
					Quantity :
					<select>
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
						<option>7</option>
						<option>8</option>
						<option>9</option>
						<option>10</option>
					</select>
				</h6>
				<a href="#" class="btn btn-default navbar-btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Add to Cart</a>
				<a href="#" class="btn btn-default navbar-btn buy">Buy Now</a>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="faq-container">
				  <div class="faq active">
				    <h3 class="faq-title">How to Apply? / কিভাবে ব্যবহার করবেন?</h3>
				    
				    <p class="faq-text">
				      Clean your face with Natural Glow Facewash of Rachna Care every night. Then dry your skin with a fresh towel. After that apply Saffron Aloevera Gel on your face with Upward Stroke.<br>
					  প্রতিদিন রাত্রে Rachna Care এর Natural Glow faceewash দিয়ে মুখ পরিষ্কার করে শুকনো তোয়ালে দিয়ে মুছে নিন। তারপর পরিমাণ মত Saffron Aloevera Gel নিয়ে Upward Stroke এ ব্যবহার করুন।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				  
				  <div class="faq">
				    <h3 class="faq-title">Composition / উপকরণ
				</h3>
				    
				    <p class="faq-text">
				      Aloevera, Saffron.<br>অ্যালোভেরা, জাফরান।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				  
				  <div class="faq">
				    <h3 class="faq-title">Why use it? / কেন ব্যবহার করবেন?
				</h3>
				    
				    <p class="faq-text">
				      It soothes and moisturizes the skin. Prevents rash, acne and pimples. Skin will be glowing and smooth.<br>Male and female both can use this product.<br>
				      নিয়মিত ব্যবহার করলে এটি ত্বকের জেল্লা ধরে রাখে, ত্বককে করে আর্দ্র ও কোমল এবং ত্বককে ফুসকুড়ি, ব্রণ থেকে রক্ষা করে। ত্বক হয়ে উঠবে ঊজ্জ্বল ও মসৃণ।<br>নারী, পুরুষ নির্বিশেষে সবাই ব্যবহার করতে পারবেন।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				</div>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<!-- <div class="blank_video">
					<iframe src="https://www.youtube.com/embed/9YzYaCuR8vk" title="Rachna Care Product // Natural Glow Facewash // Beauty Talk Show // Mitali Care Plus@mitaligroup" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
				</div> -->	
			</div>
		</div>
	</div>
</section>