<?php
session_start();
include("dbconn/dbconn.php");
$conn = connectToMySQL();
$sql = "SELECT uniqueid, productname, productdetails, category, subcategory, price,quantity,images, created FROM product";
$result = $conn->query($sql);
?>

<section class="featured padding-top padding-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-6">
				<h2>Our Featured Products</h2>

			</div>
			<?php
			if ($result->num_rows > 0) {
				while ($row = $result->fetch_assoc()) {
			?>
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-6 main">
						<div class="product">
							<div class="pro-1">
								<ul>
									<?php

									$images = json_decode($row['images'], true);

									foreach ($images as $image) {
										$imagePath = "../uploads/" . $image;
										echo '<li onclick="document.getElementById(\'myImage\').src=\'' . $imagePath . '\'">';
										echo '<img src="' . $imagePath . '" alt="img" />';
										echo '</li>';
									}
									?>
								</ul>
							</div>
							<div class="pro-2">
								<div class="rectangle"></div>
								<img id="myImage" src="<?php echo $imagePath; ?>" alt="img" />
							</div>
						</div>
						<div class="name">
							<h5><?php echo $row['productname']; ?></h5>
							<h6>Rs. <span><?php echo $row['price']; ?></span> / Quantity <span><?php echo $row['quantity']; ?></span></h6>
							<a href="#" class="btn btn-default navbar-btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Add to Cart</a>
							<a href="#" class="btn btn-default navbar-btn buy">Buy Now</a>
							<a href='productdetails.php?id=<?php echo $row["uniqueid"]; ?>' class="btn btn-default navbar-btn view"><i class="fa fa-eye" aria-hidden="true"></i>View Product Details</a>

						</div>
					</div>
			<?php
				}
			} else {
				echo "No products found.";
			}

			?>
			<!-- hello world  this is the end of the world -->
		</div>
	</div>
</section>