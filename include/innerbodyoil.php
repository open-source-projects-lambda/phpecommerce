<section class="bodyoil padding-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-lg-5 col-md-7 col-sm-12 col-xs-12">
				<div class="product">
					<div class="pro-1">
						<ul>
							<li onclick="document.getElementById('myImage').src='img/Product-2.jpg'">
								<img src="img/Product-2.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-2-img-2.jpg'">
								<img src="img/Product-2-img-2.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-2-img-3.jpg'">
								<img src="img/Product-2-img-3.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-2-img-4.jpg'">
								<img src="img/Product-2-img-4.jpg" alt="img"/>
							</li>
						</ul>
					</div>
					<div class="pro-2">
						<div class="rectangle"></div>
						<img id="myImage" src="img/Product-2.jpg" alt="img"/>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-lg-7 col-md-5 col-sm-12 col-xs-12">
				<h2>Natural Body Oil for men & women</h2>
				<h5>Brand : Rachna Care / Sold by : Mitali Care Plus</h5>
				<h6>Rs. <span>85</span> / Weight : <span>100 ml</span></h6>
				<h6>Reviews 0</h6>
				<div class="line"></div>
				<h6>Expiry Date : 10/2026</h6>
				<h6>Suitable for : All skin Type</h6>
				<h6>
					Quantity :
					<select>
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
						<option>7</option>
						<option>8</option>
						<option>9</option>
						<option>10</option>
					</select>
				</h6>
				<a href="#" class="btn btn-default navbar-btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Add to Cart</a>
				<a href="#" class="btn btn-default navbar-btn buy">Buy Now</a>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="faq-container">
				  <div class="faq active">
				    <h3 class="faq-title">How to Apply? / কিভাবে ব্যবহার করবেন?</h3>
				    
				    <p class="faq-text">
				      After bath, take some amount of oil and message whole body with it.<br>স্নান করার পর পরিমাণমত নিয়ে সারা শরীরে ম্যাসাজ করতে হবে।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				  
				  <div class="faq">
				    <h3 class="faq-title">Composition / উপকরণ
				</h3>
				    
				    <p class="faq-text">
				      Sesame Oil, Sandalwood Oil, Olive Oil, Groundnut Oil, Manjistha, Daruharidra, Vitamin E<br>তিল তেল, স্যান্ডালউড অয়েল, অলিভ অয়েল, গ্রাউন্ডনাট অয়েল, মঞ্জিষ্ঠা, দারুহরিদ্রা, ভিটামিন E
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				  
				  <div class="faq">
				    <h3 class="faq-title">Why use it? / কেন ব্যবহার করবেন?
				</h3>
				    
				    <p class="faq-text">
				      Skin will be glowing, smooth and fresh.<br> Male and female both can use this product.<br>
				      নিয়মিত ব্যবহারে ত্বক হয়ে উঠবে ঊজ্জ্বল, মসৃণ এবং প্রাণবন্ত।<br>নারী, পুরুষ নির্বিশেষে সবাই ব্যবহার করতে পারবেন।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				</div>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="blank_video">
					<iframe src="https://www.youtube.com/embed/5CIhaTkc3sA" title="Rachna Care Product // Natural Body Oil // Beauty Talk Show // MItali Care Plus @mitaligroup" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
				</div>	
			</div>
		</div>
	</div>
</section>