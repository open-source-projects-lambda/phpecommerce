<section class="ubtan padding-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-lg-5 col-md-7 col-sm-12 col-xs-12">
				<div class="product">
					<div class="pro-1">
						<ul>
							<li onclick="document.getElementById('myImage').src='img/Product-7.jpg'">
								<img src="img/Product-7.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-7-img-2.jpg'">
								<img src="img/Product-7-img-2.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-7-img-3.jpg'">
								<img src="img/Product-7-img-3.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-7-img-4.jpg'">
								<img src="img/Product-7-img-4.jpg" alt="img"/>
							</li>
						</ul>
					</div>
					<div class="pro-2">
						<div class="rectangle"></div>
						<img id="myImage" src="img/Product-7.jpg" alt="img"/>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-lg-7 col-md-5 col-sm-12 col-xs-12">
				<h2>Natural Ubtan Powder for men & women</h2>
				<h5>Brand : Rachna Care / Sold by : Mitali Care Plus</h5>
				<h6>Rs. <span>475</span> / Weight : <span>100 g</span></h6>
				<h6>Reviews 0</h6>
				<div class="line"></div>
				<h6>Expiry Date : 04/2025</h6>
				<h6>Suitable for : All skin Type</h6>
				<h6>
					Quantity :
					<select>
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
						<option>7</option>
						<option>8</option>
						<option>9</option>
						<option>10</option>
					</select>
				</h6>
				<a href="#" class="btn btn-default navbar-btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Add to Cart</a>
				<a href="#" class="btn btn-default navbar-btn buy">Buy Now</a>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="faq-container">
				  <div class="faq active">
				    <h3 class="faq-title">How to Apply? / কিভাবে ব্যবহার করবেন?</h3>
				    
				    <p class="faq-text">
				      Take a spoonful of Ubtan Powder in a bowl and mix it with Rose Water (Gulab Jol). Apply it over your cleansed skin, gently scrub and wash it off after 10-15 minutes with cold water.<br>
					  একটি পাত্রে এক চামচ ভর্তি Ubtan Powder নিয়ে গোলাপ জলের সাথে ভালো করে মেশাতে হবে। পরিষ্কার ত্বকে এই মিশ্রণটি মেখে ভালোভাবে ঘষতে হবে এবং ১০-১৫ মিনিট পর ঠান্ডা জল দিয়ে ধুয়ে নিতে হবে।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				  
				  <div class="faq">
				    <h3 class="faq-title">Composition / উপকরণ
				</h3>
				    
				    <p class="faq-text">
				      Rice flour, Almond, Oatmeal, Neem, Orange, Sandalwood, Rose, Turmeric.<br>গম, আমন্ড, ওটমিল, নিম, কমলালেবু, স্যান্ডালউড, গোলাপ, হলুদ।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				  
				  <div class="faq">
				    <h3 class="faq-title">Why use it? / কেন ব্যবহার করবেন?
				</h3>
				    
				    <p class="faq-text">
				      It detans skin, gives instant glow, removes blackheads and spots. Makes the skin smooth and rash free.<br>Male and female both can use this product.<br>
				      ট্যান পড়া থেকে ত্বককে রক্ষা করে, ত্বককে করে ঊজ্জ্বল, ত্বকের কালো দাগ সারিয়ে তোলে। ত্বককে করে মসৃণ ও rash মুক্ত।<br>নারী, পুরুষ নির্বিশেষে সবাই ব্যবহার করতে পারবেন।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				</div>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<!-- <div class="blank_video">
					<iframe src="https://www.youtube.com/embed/9YzYaCuR8vk" title="Rachna Care Product // Natural Glow Facewash // Beauty Talk Show // Mitali Care Plus@mitaligroup" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
				</div> -->	
			</div>
		</div>
	</div>
</section>