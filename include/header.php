<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Mitali Care Plus</title>
	<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"> -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Kaushan+Script&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">

	<link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/Owl Carousel2/2.2.1/assets/owl.theme.default.min.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
	<div class="wrapper">
		<header id="header">
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-xl-8 col-lg-8 col-md-9 col-sm-12 col-xs-12">
							<div class="left-part">
								<p><a href="tel:+919123859116"><i class="fa fa-phone"></i> Call us on : (+91) 9123859116 <!-- | --> </a><!-- <a href="mailto:info.mitaliitservicesopc@gmail.com"><i class="fa fa-envelope"> Mail us on : info.mitaliitservicesopc@gmail.com</i></a> --></p>
							</div>
						</div>
						<div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-xs-12">
							<div class="social">
								<a href="JavaScript:void(0);" target="_blank"><i class="fa fa-youtube-play"></i></a>
								<a href="JavaScript:void(0);" target="_blank"><i class="fa fa-instagram"></i></a>
								<a href="JavaScript:void(0);" target="_blank"><i class="fa fa-whatsapp"></i></a>
								<a href="JavaScript:void(0);" target="_blank"><i class="fa fa-facebook-f"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="menu">
				<div class="container">
					<div class="row">
						<div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
							<a href="index.php"><img src="img/Logo.png" alt="logo-image" /></a>
						</div>

						<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">
							<a href="index.php"><img class="rachna" src="img/rachnalogo.png" alt="logo-image" /></a>
							<h5>We are Authorised <br>Dealer of >></h5>
						</div>
					</div>
				</div>
			</div>
		</header>