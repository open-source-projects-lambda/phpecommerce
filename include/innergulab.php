<section class="gulab padding-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-lg-5 col-md-7 col-sm-12 col-xs-12">
				<div class="product">
					<div class="pro-1">
						<ul>
							<li onclick="document.getElementById('myImage').src='img/Product-8.jpg'">
								<img src="img/Product-8.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-8-img-2.jpg'">
								<img src="img/Product-8-img-2.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-8-img-3.jpg'">
								<img src="img/Product-8-img-3.jpg" alt="img"/>
							</li>
							<li onclick="document.getElementById('myImage').src='img/Product-8-img-4.jpg'">
								<img src="img/Product-8-img-4.jpg" alt="img"/>
							</li>
						</ul>
					</div>
					<div class="pro-2">
						<div class="rectangle"></div>
						<img id="myImage" src="img/Product-8.jpg" alt="img"/>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-lg-7 col-md-5 col-sm-12 col-xs-12">
				<h2>Natural Gulab Jol for men & women</h2>
				<h5>Brand : Rachna Care / Sold by : Mitali Care Plus</h5>
				<h6>Rs. <span>50</span> / Weight : <span>100 ml</span></h6>
				<h6>Reviews 0</h6>
				<div class="line"></div>
				<h6>Expiry Date : 06/2025</h6>
				<h6>Suitable for : All skin Type</h6>
				<h6>
					Quantity :
					<select>
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
						<option>7</option>
						<option>8</option>
						<option>9</option>
						<option>10</option>
					</select>
				</h6>
				<a href="#" class="btn btn-default navbar-btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Add to Cart</a>
				<a href="#" class="btn btn-default navbar-btn buy">Buy Now</a>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="faq-container">
				  <div class="faq active">
				    <h3 class="faq-title">How to Apply? / কিভাবে ব্যবহার করবেন?</h3>
				    
				    <p class="faq-text">
				      After taking bath, apply a little amount of Rose Water with cotton or with Ubtan Powder.<br>
					  প্রতিদিন স্নান করার পর তুলোতে সামান্য পরিমাণ নিয়ে অথবা Ubtan Powder এর সাথে মিশিয়ে লাগাতে হবে।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				  
				  <div class="faq">
				    <h3 class="faq-title">Composition / উপকরণ
				</h3>
				    
				    <p class="faq-text">
				      Rose, Fresh water.<br>গোলাপ, জল।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				  
				  <div class="faq">
				    <h3 class="faq-title">Why use it? / কেন ব্যবহার করবেন?
				</h3>
				    
				    <p class="faq-text">
				      Cleansing, Toning, Instant Hydrating.<br>Male and female both can use this product.<br>
				      ক্লিনজিং, টোনিং, ইনস্ট্যান্ট হাইড্রেটিং।<br>নারী, পুরুষ নির্বিশেষে সবাই ব্যবহার করতে পারবেন।
				    </p>
				    <button class="faq-toggle">
				      <i class="fa fa-plus"></i>
				    <i class="fa fa-times"></i>
				    </button>
				  </div>
				</div>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<!-- <div class="blank_video">
					<iframe src="https://www.youtube.com/embed/9YzYaCuR8vk" title="Rachna Care Product // Natural Glow Facewash // Beauty Talk Show // Mitali Care Plus@mitaligroup" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
				</div> -->	
			</div>
		</div>
	</div>
</section>