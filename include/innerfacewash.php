<?php
session_start();
$uid = $_GET["id"];
include("dbconn/dbconn.php");
$conn = connectToMySQL();
$sql = "SELECT * from product where uniqueid='$uid'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
?>
<section class="face padding-bottom">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-lg-5 col-md-7 col-sm-12 col-xs-12">
				<div class="product">
					<div class="pro-1">
						<ul>
							<?php

							$images = json_decode($row['images'], true);

							foreach ($images as $image) {
								$imagePath = "../uploads/" . $image;
								echo '<li onclick="document.getElementById(\'myImage\').src=\'' . $imagePath . '\'">';
								echo '<img src="' . $imagePath . '" alt="img" />';
								echo '</li>';
							}
							?>
						</ul>
					</div>
					<div class="pro-2">
						<div class="rectangle"></div>
						<?php
						$imagePath2 = "../uploads/" . $image;
						$image1 = json_decode($row['images'], true);

						echo '<img id="myImage" src="' . $imagePath2 . '" alt="img" />';
						?>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-lg-7 col-md-5 col-sm-12 col-xs-12">
				<h2><?php echo $row["productname"] ?></h2>
				<h5>Brand : Rachna Care / Sold by : Mitali Care Plus</h5>
				<h6>Rs. <span><?php echo $row['price'] ?></span> / Weight : <span><?php echo $row["quantity"] ?></span></h6>
				<h6>Reviews 0</h6>
				<div class="line"></div>
				<h6>Expiry Date : <?php echo $row['expirydate'] ?></h6>
				<h6>Suitable for : All skin Type</h6>

				<a href="#" class="btn btn-default navbar-btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Add to Cart</a>
				<a href="#" class="btn btn-default navbar-btn buy">Buy Now</a>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="faq-container">
					<div class="faq active">
						<!-- <h3 class="faq-title">How to Apply? / কিভাবে ব্যবহার করবেন?</h3> -->

						<p class="faq-text">
							<?php echo $row["productdetails"] ?>
						</p>
						<button class="faq-toggle">
							<i class="fa fa-plus"></i>
							<i class="fa fa-times"></i>
						</button>
					</div>

					<!-- <div class="faq">
						<h3 class="faq-title">Composition / উপকরণ
						</h3>

						<p class="faq-text">
							Neem, Tulsi, Turmeric, Oats, Manjistha, Daruharidra, Olive Oil, Wheet Germ Oil, Glycerin.<br>নিম, তুলসী, হলুদ, ওটস, মঞ্জিষ্ঠা, দারুহরিদ্রা, অলিভ অয়েল, হুইটজার্ম অয়েল, গ্লিসারিন।
						</p>
						<button class="faq-toggle">
							<i class="fa fa-plus"></i>
							<i class="fa fa-times"></i>
						</button>
					</div> -->

					<!-- <div class="faq">
						<h3 class="faq-title">Why use it? / কেন ব্যবহার করবেন?
						</h3>

						<p class="faq-text">
							Skin will be glowing, smooth and fresh.<br> Male and female both can use this product.<br>
							নিয়মিত ব্যবহারে ত্বক হয়ে উঠবে ঊজ্জ্বল, মসৃণ এবং প্রাণবন্ত।<br>নারী, পুরুষ নির্বিশেষে সবাই ব্যবহার করতে পারবেন।
						</p>
						<button class="faq-toggle">
							<i class="fa fa-plus"></i>
							<i class="fa fa-times"></i>
						</button>
					</div> -->

					<!-- <div class="faq">
						<h3 class="faq-title">Extra Benefit / অতিরিক্ত সুবিধা
						</h3>

						<p class="faq-text">
							Males can use it as Shaving Cream instead of foams.<br>পুরুষরা foam এর পরিবর্তে এটি শেভিং ক্রিম হিসাবে ব্যবহার করতে পারবেন।
						</p>
						<button class="faq-toggle">
							<i class="fa fa-plus"></i>
							<i class="fa fa-times"></i>
						</button>
					</div> -->
				</div>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="blank_video">
					<iframe src="https://www.youtube.com/embed/9YzYaCuR8vk" title="Rachna Care Product // Natural Glow Facewash // Beauty Talk Show // Mitali Care Plus@mitaligroup" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>