<?php
session_start();
include("../../dbconn/dbconn.php");
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    echo "";
} else {
    header("Location: ../auth/login.php");
}
$conn = connectToMySQL();
if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $uid = $_GET['id'];
    $sql = "DELETE FROM category WHERE id=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $uid);
    if ($stmt->execute()) {

        header("Location: editcategory.php");
        exit();
    } else {

        echo "Error deleting product: " . $conn->error;
    }
} else {

    echo "Invalid ID provided";
}
