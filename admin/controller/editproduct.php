<?php
session_start();
include("../../dbconn/dbconn.php");
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    echo "";
} else {
    header("Location: ../auth/login.php");
}
$conn = connectToMySQL();
$sql = "SELECT uniqueid, productname, productdetails, category, subcategory, price,quantity,expirydate,images, created FROM product";
$sql2 = "SELECT compute from productanalytics";
$result = $conn->query($sql);
$result2 = $conn->query($sql2);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modify product</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }

        table,
        th,
        td {
            border: 1px solid #ddd;
            padding: 8px;
        }

        th {
            background-color: #f2f2f2;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        tr:hover {
            background-color: #ddd;
        }

        td img {
            max-width: 100px;
            max-height: 100px;
            margin-right: 15px;
        }
    </style>
</head>

<body>
    <a href="../index.php">Go back to Dashboard</a>
    <br>
    <br>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Product Name</th>
                <th>Product Details</th>
                <th>In stock</th>
                <th>Category</th>
                <th>Subcategory</th>
                <th>Price</th>
                <th>Quantity</th>

                <th>Images</th>
                <th>Expiry Date</th>

                <th>Created</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($result->num_rows > 0 && $result2->num_rows > 0) {
                while (($row = $result->fetch_assoc()) && ($row2 = $result2->fetch_assoc())) {
                    echo "<tr>";
                    echo "<td>" . $row["uniqueid"] . "</td>";
                    echo "<td>" . $row["productname"] . "</td>";
                    echo "<td>" . $row["productdetails"] . "</td>";
                    echo "<td>" . $row2["compute"] . "</td>";
                    echo "<td>" . $row["category"] . "</td>";
                    echo "<td>" . $row["subcategory"] . "</td>";
                    echo "<td>" . $row["price"] . "</td>";
                    echo "<td>" . $row["quantity"] . "</td>";
                    $images = json_decode($row["images"], true);
                    echo "<td>";
                    foreach ($images as $image) {
                        echo "<img src='../../uploads/$image' alt='Product Image'>";
                    }
                    echo "</td>";
                    echo "<td>" . $row['expirydate'] . "</td>";
                    echo "<td>" . date('Y-m-d', strtotime($row["created"])) . "</td>";
                    echo "<td><a href='modifyproduct.php?id=" . $row["uniqueid"] . "'>Edit</a></td>";
                    echo "<td><a href='deleteproduct.php?id=" . $row["uniqueid"] . "'>Delete</a></td>";
                    echo "</tr>";
                }
            } else {
                echo "<tr><td colspan='8'>No products found</td></tr>";
            }
            ?>
        </tbody>
    </table>
</body>

</html>