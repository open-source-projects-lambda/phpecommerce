<?php
session_start();
include('../../dbconn/dbconn.php');

if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    echo "";
} else {
    header("Location: ../auth/login.php");
}
$conn = connectToMySQL();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (
        isset($_POST['productname']) && isset($_POST['productdetails']) && isset($_POST['category']) && isset($_POST['subcategory']) && isset($_POST['pricing'])
    ) {
        $productname = $_POST['productname'];
        $productdetails = $_POST['productdetails'];
        $category = $_POST['category'];
        $subcategory = $_POST['subcategory'];
        $pricing = $_POST['pricing'];
        $quantity = $_POST['quantity'];
        $exprirydate = $_POST['expirydate'];
        $compute = $_POST['compute'];
        $error = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        $imageNames = array();
        if (!empty($_FILES["files"]["name"][0])) {
            foreach ($_FILES["files"]["tmp_name"] as $key => $tmp_name) {
                $file_name = $_FILES["files"]["name"][$key];
                $file_tmp = $_FILES["files"]["tmp_name"][$key];
                $ext = pathinfo($file_name, PATHINFO_EXTENSION);

                if (in_array($ext, $extension)) {
                    $newFileName = basename($file_name, $ext) . time() . "." . $ext;
                    $destination = "../../uploads/" . $newFileName;
                    if (move_uploaded_file($file_tmp, $destination)) {
                        $imageNames[] = $newFileName;
                    } else {
                        $error[] = "Failed to upload $file_name";
                    }
                } else {
                    $error[] = "Invalid file type: $file_name";
                }
            }
        } else {
            // No images uploaded
            $error[] = "Please upload at least one image";
        }
        $productimages = json_encode($imageNames);
        $uniqid = time();
        $stmt = $conn->prepare("INSERT INTO product (uniqueid, productname, productdetails, category, subcategory, price, quantity, expirydate ,images, created) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())");
        $stmt->bind_param("sssssssss", $uniqid, $productname, $productdetails, $category, $subcategory, $pricing, $quantity, $exprirydate, $productimages);
        $stmt->execute();
        $stmt2 = $conn->prepare("INSERT INTO productanalytics(uniqueid,compute,created) VALUES(?,?,NOW())");
        $stmt2->bind_param("ss", $uniqid, $compute);
        $stmt2->execute();
        $stmt3 = $conn->prepare("UPDATE category SET uniqueid=? WHERE category=?");
        $stmt3->bind_param("ss", $uniqid, $category);
        $stmt3->execute();



        if ($stmt->affected_rows > 0 && $stmt2->affected_rows > 0 && $stmt3->affected_rows > 0) {
            echo "Product added successfully.";
        } else {
            echo "Error adding product." . $conn->error;
        }

        $stmt->close();
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add product</title>
    <style>
        /* Apply flex layout to center the form */
        body,
        html {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        /* Basic styling for the form */
        form {
            margin: 20px;
            padding: 20px;
            border: 1px solid #ccc;
            width: 300px;
        }

        input[type="text"],
        textarea,
        input[type="file"],
        input[type="submit"] {
            margin-bottom: 10px;
            width: 100%;
            padding: 8px;
        }

        input[type="submit"] {
            background-color: blueviolet;
            color: white;
            border: none;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: purple;
        }
    </style>
</head>

<body>
    <form action="../controller/addproduct.php" method="post" enctype="multipart/form-data">
        <h2>Add Product</h2>

        <label for="productname">Product Name:</label>
        <input type="text" id="productname" name="productname" required>

        </p>
        <label for="productdetails">Product Details:</label>
        <textarea id="productdetails" name="productdetails" rows="4" cols="50">
</textarea>

        <label for="sizes">Product Customization:</label>
        <br>
        <br>
        <select id="category" name="category">
            <option value="" selected>Select Category</option>
            <?php

            $sql = "SELECT DISTINCT category FROM category";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($category);

            while ($stmt->fetch()) {
                echo "<option value='$category'>$category</option>";
            }

            if ($stmt->num_rows === 0) {
                echo "<option value=''>No categories found</option>";
            }

            ?>

        </select>
        <select id="subcategory" name="subcategory">
            <option value="" selected>Select SubCategory</option>
            <?php

            $sql = "SELECT DISTINCT subcategory FROM category";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($subcategory);

            while ($stmt->fetch()) {
                echo "<option value='$subcategory'>$subcategory</option>";
            }

            if ($stmt->num_rows === 0) {
                echo "<option value=''>No categories found</option>";
            }

            ?>

        </select>
        <br>
        <br>
        <!-- Price inputs for different subcategories -->
        <div id="prices">
            <label for="price">Price:</label>
            <input type="text" id="pricing" name="pricing" placeholder="give a INR value">
        </div>
        <br>
        <div id="quantity">
            <label for="quantity">Product Weight :(ex: ml,liter,kg or g)</label>
            <input type="text" id="quantity" name="quantity" placeholder="give a quantity value ex 100ml">
        </div>
        <label for="compute">Product Quantity :(ex: 10,20,100 etc)</label>
        <input type="text" id="compute" name="compute" placeholder="give a quantity value ex 10">

        <label for="productimage">Product Image:</label>
        <input type="file" name="files[]" required multiple>
        <label for="quantity">Expiry Date (ex: 01/2034)</label>
        <input type="text" id="expirydate" name="expirydate" placeholder="Expiry Date (ex: 01/2034)">
        <input type="submit" value="Add Product" id="submitBtn">
        <div class="container">
            <a href="../index.php">Goto Dashboard</a>
        </div>
        <br>
        <div class="container">
            <a href="../controller/addcategory.php">Add new category</a>
        </div>
        <br>
        <div class="container">
            <a href="../controller/editproduct.php">Edit product</a>
        </div>
    </form>
</body>

</html>