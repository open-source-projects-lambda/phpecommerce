<?php
session_start();
include("../../dbconn/dbconn.php");
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    echo "";
} else {
    header("Location: ../auth/login.php");
}
$conn = connectToMySQL();
$sql = "SELECT id ,category, subcategory, created from category";
$result = $conn->query($sql);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modify category</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }

        table,
        th,
        td {
            border: 1px solid #ddd;
            padding: 8px;
        }

        th {
            background-color: #f2f2f2;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        tr:hover {
            background-color: #ddd;
        }

        td img {
            max-width: 100px;
            max-height: 100px;
            margin-right: 15px;
        }
    </style>
</head>

<body>
    <a href="../index.php">Go back to Dashboard</a>
    <br>
    <br>
    <table>
        <thead>
            <tr>
                <!-- <th>ID</th> -->
                <th>Category</th>
                <th>Subcategory</th>
                <th>Created</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
            <?php

            if ($result->num_rows > 0) {

                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $row["category"] . "</td>";
                    echo "<td>" . $row["subcategory"] . "</td>";
                    echo "<td>" . date('Y-m-d', strtotime($row["created"])) . "</td>";

                    echo "<td><a href='modifycategory.php?id=" . $row["id"] . "'>Edit</a></td>";
                    echo "<td><a href='deletecategory.php?id=" . $row["id"] . "'>Delete</a></td>";
                    echo "</tr>";
                }
            } else {

                echo "<tr><td colspan='6'>No data found in the category table</td></tr>";
            }
            ?>
        </tbody>

</body>

</html>