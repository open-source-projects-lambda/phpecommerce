<?php
session_start();
include("../../dbconn/dbconn.php");
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    echo "";
} else {
    header("Location: ./auth/login.php");
}

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $conn = connectToMySQL();
    $uid = $_GET['id'];

    $sql = "DELETE FROM product WHERE uniqueid=?";
    $stmt = $conn->prepare($sql);

    $stmt->bind_param("i", $uid);


    if ($stmt->execute()) {

        header("Location: editproduct.php");
        exit();
    } else {

        echo "Error deleting product: " . $conn->error;
    }
} else {

    echo "Invalid ID provided";
}
