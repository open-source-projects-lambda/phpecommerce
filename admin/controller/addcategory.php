<?php
session_start();
include('../../dbconn/dbconn.php');

if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    echo "";
} else {
    header("Location: ../auth/login.php");
}
$conn = connectToMySQL();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['categoryName']) && isset($_POST['subCategoryName'])) {
        $categoryname = $_POST['categoryName'];
        $subcategory = $_POST['subCategoryName'];
        $sql = "INSERT into category(category,subcategory,created)VALUES(?,?,NOW())";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ss", $categoryname, $subcategory);
        if ($stmt->execute()) {
            echo "Category addedd successfully";
        } else {
            echo "Failed there is some error at server";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add category</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        form {
            max-width: 400px;
            margin: 20px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .form-group {
            margin-bottom: 20px;
        }

        label {
            display: block;
            margin-bottom: 5px;
        }

        input[type="text"],
        input[type="submit"] {
            width: 100%;
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type="submit"] {
            background-color: #4CAF50;
            color: white;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #45a049;
        }
    </style>
</head>

<body>
    <form id="categoryForm" action="addcategory.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="categoryName">Category Name:</label>
            <input type="text" id="categoryName" name="categoryName" required>
        </div>
        <div class="form-group">
            <label for="subCategoryName">Subcategory Name</label>
            <input type="text" id="subCategoryName" name="subCategoryName" required>
        </div>
        <input type="submit" id="submitBtn" name="submitBtn" value="Submit">
        <br>
        <br>
        <div class="container">
            <a href="../index.php">Goto Dashboard</a>
        </div>
        <br>
        <div class="container">
            <a href="../controller/addproduct.php">Add a new product</a>
        </div>
    </form>

</body>

</html>