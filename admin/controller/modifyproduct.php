<?php
session_start();
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    echo "";
} else {
    header("Location: ./auth/login.php");
}
include("../../dbconn/dbconn.php");
$uid = $_GET["id"];
$conn = connectToMySQL();
$sql = "SELECT * from product where uniqueid='$uid'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$sql3 = "SELECT * from productanalytics where uniqueid='$uid'";
$result3 = $conn->query($sql3);
$row2 = $result3->fetch_assoc();

if (isset($_POST["category"])) {
    $uniqueid = $_POST["uniqueid"];
    $productname = $_POST["productname"];
    $productdetails = $_POST["productdetails"];
    $category = $_POST["category"];

    $subcategory = $_POST["subcategory"];
    $price = $_POST["price"];
    $quantity = $_POST["quantity"];
    $exprirydate = $_POST['expirydate'];
    $compute = $_POST['compute'];
    $productname = mysqli_real_escape_string($conn, $productname);
    $productdetails = mysqli_real_escape_string($conn, $productdetails);
    $category = mysqli_real_escape_string($conn, $category);
    $subcategory = mysqli_real_escape_string($conn, $subcategory);
    $price = mysqli_real_escape_string($conn, $price);
    $quantity = mysqli_real_escape_string($conn, $quantity);
    $sql2 = "UPDATE product SET productname='$productname', productdetails='$productdetails', category='$category', subcategory='$subcategory', price='$price',quantity='$quantity', expirydate='$exprirydate' WHERE uniqueid='$uniqueid'";
    $sql4 = "UPDATE productanalytics SET compute='$compute' WHERE uniqueid='$uniqueid'";
    if ($conn->query($sql2) === TRUE && $conn->query($sql4) === TRUE) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $conn->error;
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit or Modify product</title>
    <style>
        body,
        html {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        form {
            margin: 20px;
            padding: 20px;
            border: 1px solid #ccc;
            width: 300px;
        }

        input[type="text"],
        textarea,
        input[type="file"],
        input[type="submit"] {
            margin-bottom: 10px;
            width: 100%;
            padding: 8px;
        }

        input[type="submit"] {
            background-color: blueviolet;
            color: white;
            border: none;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: purple;
        }
    </style>
</head>

<body>
    <h2>Edit Product</h2>
    <form action="modifyproduct.php?id=<?php echo $row["uniqueid"]; ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="uniqueid" value="<?php echo $row['uniqueid']; ?>">
        <label for="productname">Product Name:</label><br>
        <input type="text" id="productname" name="productname" value="<?php echo $row['productname']; ?>"><br>
        <label for="productdetails">Product Details:</label><br>
        <textarea id="productdetails" name="productdetails"><?php echo $row['productdetails']; ?></textarea><br>
        <label for="category">Category:</label><br>
        <select id="category" name="category">
            <option value="" selected>Select Category</option>
            <?php


            $product_uniqueid = $row['uniqueid'];

            $sql = "SELECT DISTINCT category, uniqueid FROM category";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($category, $uniqueid);

            while ($stmt->fetch()) {
                $selected = ($uniqueid == $product_uniqueid) ? "selected" : "";
                echo "<option value='$category' $selected>$category</option>";
            }

            if ($stmt->num_rows === 0) {
                echo "<option value=''>No categories found</option>";
            }

            ?>

        </select>
        <br>
        <br>
        <label for="subcategory">Subcategory:</label><br>
        <select id="subcategory" name="subcategory">
            <option value="" selected>Select SubCategory</option>
            <?php
            $product_uniqueid = $row['uniqueid'];
            $sql = "SELECT DISTINCT subcategory , uniqueid FROM category";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $stmt->bind_result($subcategory, $uniqueid);

            while ($stmt->fetch()) {
                $selected = ($uniqueid == $product_uniqueid) ? "selected" : "";
                echo "<option value='$subcategory' $selected>$subcategory</option>";
            }

            if ($stmt->num_rows === 0) {
                echo "<option value=''>No categories found</option>";
            }

            ?>

        </select>
        <br>
        <br>
        <label for="price">Price:</label><br>
        <input type="text" id="price" name="price" value="<?php echo $row['price']; ?>"><br>
        <label for="quantity">Product Quantity :(ex: ml,liter,kg or g)</label>
        <input type="text" id="quantity" name="quantity" placeholder="give a quantity value ex 100ml" value="<?php echo $row['quantity']; ?>"><br>
        <label for="compute">Product Quantity :(ex: 10,20,100 etc)</label>
        <input type="text" id="compute" name="compute" placeholder="give a quantity value ex 10" value="<?php echo $row2['compute'] ?>">

        <label for="quantity">Expiry Date (ex: 01/2034)</label>
        <input type="text" id="expirydate" name="expirydate" placeholder="Expiry Date (ex: 01/2034)" value="<?php echo $row['expirydate'] ?>">
        <input type="submit" value="Submit">
        <br>
        <br>
        <a href="editproduct.php">Goto previouspage</a>
    </form>
</body>

</html>