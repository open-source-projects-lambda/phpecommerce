<?php
session_start();
include("../../dbconn/dbconn.php");
$conn = connectToMySQL();

$username = isset($_POST["userid"]) ? $_POST["userid"] : "";
$password = isset($_POST["password"]) ? $_POST["password"] : "";

$sql = "SELECT * FROM admin WHERE userid = ? AND password = ?";
$stmt = $conn->prepare($sql);

if (!empty($username) && !empty($password)) {
    $stmt->bind_param("ss", $username, $password);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($result->num_rows == 1) {
        $_SESSION['loggedin'] = true;
        header("Location: ../index.php");
    } else {
        echo "Incorrect username or password";
    }
} else {

    echo "Username or password not provided";
}

$stmt->close();
$conn->close();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Auth</title>
    <link rel="stylesheet" href="css/style.css">

</head>

<body>
    <div class="container">
        <div class="screen">
            <div class="screen__content">
                <form class="login" action="login.php" method="POST" enctype="multipart/form-data">
                    <div class="login__field">
                        <i class="login__icon fas fa-user"></i>
                        <input type="text" name="userid" id="userid" class="login__input" placeholder="User name / Email">
                    </div>
                    <div class="login__field">
                        <i class="login__icon fas fa-lock"></i>
                        <input type="password" name="password" id="password" class="login__input" placeholder="Password">
                    </div>
                    <button type="submit" class="button login__submit">
                        <span class="button__text">Log In Now</span>
                        <i class="button__icon fas fa-chevron-right"></i>
                    </button>
                </form>
                <div class="social-login">
                    <h3>log in via</h3>
                    <div class="social-icons">
                        <a href="#" class="social-login__icon fab fa-instagram"></a>
                        <a href="#" class="social-login__icon fab fa-facebook"></a>
                        <a href="#" class="social-login__icon fab fa-twitter"></a>
                    </div>
                </div>
            </div>
            <div class="screen__background">
                <span class="screen__background__shape screen__background__shape4"></span>
                <span class="screen__background__shape screen__background__shape3"></span>
                <span class="screen__background__shape screen__background__shape2"></span>
                <span class="screen__background__shape screen__background__shape1"></span>
            </div>
        </div>
    </div>

</body>

</html>