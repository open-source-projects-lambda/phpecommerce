<?php


function connectToMySQL()
{
    $db_host =  '';
    $db_user = '';
    $db_pass = '';
    $db_name = '';
    $db_port = '';
    $conn = mysqli_connect($db_host, $db_user, $db_pass, $db_name, $db_port);
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    return $conn;
}
